package com.example.basics.data;

import java.math.BigDecimal;//zamiast tych 2 wystarcz import java.math.*;
import java.math.BigInteger;

public class BigIntegerBigDevimal { //innymi slowy bedzie to tworzenie wartosci z tekstu i doklejanie do niego polecen
    public static void main(String[] args) {

        BigInteger bigInt = new BigInteger("9999999999999999999999999");
        bigInt = bigInt.add(new BigInteger("11111111111111111111111"));
        //BigInteger bigInt = new BigInteger("5").add(new BigInteger("10"));// to to samo, tylko jednow jednym
        System.out.println("Wynik: " + bigInt.toString());

        BigDecimal decimal = new BigDecimal("12.345345345345345345345345345345345");
        decimal = decimal.pow(3);
        decimal = decimal.subtract(new BigDecimal("234324262435235.235235235"));
        System.out.println("wynik: " + decimal);
    }
}
