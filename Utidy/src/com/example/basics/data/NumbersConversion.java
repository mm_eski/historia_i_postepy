package com.example.basics.data;

public class NumbersConversion {
    public static void main(String[] args) {

        //konwersja jawwna - zawezajaca

        int i = 5;
        double d = 20.0d / (double)i;
        System.out.println(d);

        //konwersja niejawna - rozszerzajaca    // gora i dol to to samo tylko w 1 przupadku informujemy...
        int a = 4;                                 // ze zmieniamy wartosc, w 2 zmiana jest z automatu
        double b = 10.0d * a;
        System.out.println(b);

        // rzutowanie / casting z angielskiego
        double d2 = 5.5;
        int num = 10 * (int)d2; // ale w ta strony z double do int trzeba zrobic jawnie, 5 jest ucinana po kropce.
                                // przy rzutowaniu/ konwersji float/double na typ calkowity czesc ulamkowa jest tracona
        System.out.println(num);// 50

        short small = (byte)200; // rzutowanie z short na byte co powoduje utrate inforamcji
        System.out.println("small: " + small);


    }
}
