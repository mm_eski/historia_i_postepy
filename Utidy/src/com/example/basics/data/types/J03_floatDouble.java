package com.example.basics.data.types;

public class J03_floatDouble {
    public static void main(String[] args) {
        float number = 10.5f;
        double bigNumber = 23.45454545;

        System.out.println("number: " + number
                + " bigNumber: " + bigNumber);

    }
}
