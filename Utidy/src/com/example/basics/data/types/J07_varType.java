package com.example.basics.data.types;

public class J07_varType {
    public static void main(String[] args) {
        int number = add(10,"5");
        System.out.println(number);

        float koper = minus( "20", 4);
        System.out.println(koper);

    }

    public static int add(int a, String b){
        var result = a + Integer.valueOf(b).intValue();
        return result;

    }

    public static int minus(String c, int d){
        var result = Integer.valueOf(c).intValue() - d;
        return result;
    }

}
