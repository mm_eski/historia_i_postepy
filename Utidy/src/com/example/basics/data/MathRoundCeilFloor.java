package com.example.basics.data;

public class MathRoundCeilFloor {
    public static void main(String[] args) {

        //zaokaglenie do najblizszej liczny calkowitej
        System.out.println(Math.round(5.51));   //6
        System.out.println(Math.round(5.49));   //5

        //rzutowanie, utrata czesci ulamkowej
        System.out.println( (int) 5.45d);

        //Math.ceil() - najwieksza mozliw. liczba calkowita
        System.out.println(Math.ceil(6.11)); //7.0
        System.out.println(Math.ceil(6.8)); //7.0

        //Math.floor() - najnizsza mozliwa calkowita liczba
        System.out.println(Math.floor(6.11)); //6
        System.out.println(Math.floor(6.8)); //6.0


    }
}
