package com.example.basics.data;

public class EnumExample {

    enum SoundVolume {LOW, MEDIUM, HIGHT, VERY_HIGHT};
    enum CarVariant {SEDAN, COMBI, SUPER_CAR};


    public static void main(String[] args) {
        SoundVolume phoneSound = SoundVolume.LOW;
        System.out.println(phoneSound);

    }
}
